
import matplotlib
matplotlib.use('TkAgg')

import pylab as PL
import random as RD
import scipy as SP

RD.seed()

width = 100
height = 100
initProb = 0.5 # poczatkowy rozklad prawdopodobientstwo biale albo czarne
Ra = 1
Ri = 5
Wa = 1
Wi = 0.1


def init():
    global time, config, nextConfig

    time = 0
    
    config = SP.zeros([height, width])
    for x in range(width):
        for y in range(height):
            if RD.random() < initProb:
                state = 1
            else:
                state = 0
            config[y, x] = state

    # for x in range(width/3):
    #     for y in range(height/3):
    #         if RD.random() < initProb:
    #             state = 1
    #         else:
    #             state = 0
    #         config[y+height/3, x+width/3] = state
    #
    # for x in range(width /3 ):
    #     for y in range(height/3):
    #         if RD.random() < initProb:
    #             state = 1
    #         else:
    #             state = 0
    #         config[y, x + width / 3*2] = state


    nextConfig = SP.zeros([height, width])

def draw():
    PL.cla()
    PL.pcolor(config, vmin = 0, vmax = 1, cmap = PL.cm.binary)
    PL.axis('image')
    PL.title('t = ' + str(time))

def step():
    global time, config, nextConfig

    time += 1



    for x in range(width):
        for y in range(height):
            state = config[y, x]
            na = 0
            ni = 0
            for dx in range(- Ra, Ra + 1):
                for dy in range(- Ra, Ra + 1):
                    na += config[(y+dy)%height, (x+dx)%width]
            for dx in range(- Ri, Ri + 1):
                for dy in range(- Ri, Ri + 1):
                    ni += config[(y+dy)%height, (x+dx)%width]
            if na * Wa - ni * Wi > 0:
                state = 1
            else:
                state = 0
            nextConfig[y, x] = state

    config, nextConfig = nextConfig, config

import pycxsimulatorold
pycxsimulatorold.GUI().start(func=[init,draw,step])
